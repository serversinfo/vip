#pragma semicolon 1

#include <sourcemod>
#include <vip_core>

bool ien[MAXPLAYERS+1];

public Plugin:myinfo =
{
	name = "[VIP]ShowDMG",
	author = "Darkeneez",
	version = "1.0.0",
	url = "http://vk.com/darkeneez97"
};

#define VIP_SHOWDMG			"SHOWDMG"

public VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(VIP_SHOWDMG, BOOL, _, ToggleItemCallback);
}
	
public VIP_OnVIPClientLoaded(iClient)
{
	if(VIP_IsClientFeatureUse(iClient, VIP_SHOWDMG)) ien[iClient] = true;
}
	
public Action ToggleItemCallback(iClient, const String:sFeatureName[], VIP_ToggleState:OldStatus, &VIP_ToggleState:NewStatus)
{
	if(NewStatus == ENABLED) ien[iClient] = true;
	else ien[iClient] = false;
	return Plugin_Continue;
}

public void OnPluginStart()
{
   HookEvent("player_hurt", Event_PlayerHurt);
}

public void Event_PlayerHurt(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("attacker"));
	
	if (!ien[client])
		return;
		
	PrintHintText(client, "-%d HP", event.GetInt("dmg_health"));
}